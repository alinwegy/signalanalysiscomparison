clear; close all;
addpath(genpath('.'))

%% ============== LOAD DATA ================== %%
load('alina_v2_f600.mat')
volt  = [t2dt(channel_5(:, 1)), channel_5(:,2)];
curr  = [t2dt(channel_6(:, 1)), channel_6(:,2)];
accel = [t2dt(channel_7(:, 1)), channel_7(:,2)];

ts        = 2;
te        = 3;
ts_sample = 2.5;
te_sample = 2.6;
volt      = volt(time_range(volt(:,1),ts, te), :);
curr      = curr(time_range(curr(:,1),ts, te), :);
accel     = accel(time_range(accel(:,1), ts, te), :);
X         = accel(:,1);
Y         = accel(:,2);

%% ============== PREPROCESS SIGNAL ================== %%
offset = mean(Y)
Yp = Y;
Yn = -Y;
Yp(Yp < 0) = 0;
Yn(Yn < 0) = 0;

%% ============== GET PEAKS ================== %%
[ppeaks, pindices] = findpeaks(Yp);
[npeaks, nindices] = findpeaks(Yn);
delta_ts = abs(diff(X(pindices)));
delta_ts = delta_ts(delta_ts > 0.5*mode(delta_ts));
pfreqs = 1./delta_ts;
figure
histogram(pfreqs,20);
title("Signal Frequency Histogram")
xlabel("Frequency (Hz)")
ylabel("Sample Count")
delta_ts = abs(diff(X(nindices)));
delta_ts = delta_ts(delta_ts > 0.5*mode(delta_ts));
nfreqs = 1./delta_ts;
pfreq = mode(pfreqs);
nfreq = mode(nfreqs);
freq = (pfreq + nfreq)/2
pamp  = mode(ppeaks);
figure
histogram(ppeaks,20);
title("Signal Amplitude Histogram")
xlabel("Amplitude (m/s^2)")
ylabel("Sample Count")
namp  = mode(npeaks);
amp = (pamp + namp)/2
perror = std(ppeaks - pamp);
nerror = std(npeaks - namp);
error = max(perror, nerror);
Y_pred = amp*sin(2*pi*freq*X) + offset;
[c, lag] = xcorr(Y,Y_pred);
[maxC, i] = max(c);
phase = lag(i)


%% ============== RECONSTRUCT SIGNAL ================== %%
Y_pred = amp*sin(2*pi*freq*X + phase) + offset;
figure
plot(X,Y)
hold on
plot(X, Y_pred)
hold off
