clear all; close all; clc;
addpath(genpath('.'))

%% ================== LOAD DATA ================== %%
load('alina_v2_f600.mat')
volt  = [t2dt(channel_5(:, 1)), channel_5(:,2)];
curr  = [t2dt(channel_6(:, 1)), channel_6(:,2)];
accel = [t2dt(channel_7(:, 1)), channel_7(:,2)];

ts        = 2;
te        = 3;
ts_sample = 2.5;
te_sample = 2.6;
volt      = volt(time_range(volt(:,1),ts, te), :);
curr      = curr(time_range(curr(:,1),ts, te), :);
accel     = accel(time_range(accel(:,1), ts, te), :);
X         = accel(:,1);
Y         = accel(:,2);

%% ================== LOAD DATA ================== %%
[freq, FFT] = getFFTofSignal(1/60000, Y);
snr = getSNR(FFT);
[domFreq, x1Dom, x2Dom, x3Dom, x4Dom, x5Dom] = getDomOfFFT(freq, FFT);
offset = mean(Y);
amp = getAmplitudeOfData(Y);
Y_pred = amp*sin(2*pi*domFreq*X) + offset;
[c, lag] = xcorr(Y,Y_pred);
[maxC, i] = max(c);
phase = lag(i)


%% ============== RECONSTRUCT SIGNAL ================== %%
Y_pred = amp*sin(2*pi*domFreq*X + phase) + offset;
figure
plot(X,Y)
hold on
plot(X,Y_pred)
hold off


