# SignalAnalysisComparison

This repository contains the code for the comparison between different signal analysis methods. This mainly concerns Sinesoidal signals.

The different methods being compared are:
- Frequency Response using Fast Fourier Transform (FFT) and signal to noise ratio
- Peak to peak time to measure the frequency and RMS and SD for amplitude analysis
- Fitting the data to a Sinesoidal signal

The main analysis routines are in [FitAnalyse.m](FitAnalyse.m), [FFTAnalyse.m](FFTAnalyse.m) and [P2PAnalyse.m](P2PAnalyse.m), the [SineFit.m](SineFit.m) routine is a helper function for fitting data to a sine wave provided by **R P**[^1] for fitting data to a sine wave. [json2mat.py](json2mat.py) is a file that helps convert data from the json format to mat files supported by Matlab.

---
[^1]: R P (2024). Sine function fit [https://www.mathworks.com/matlabcentral/fileexchange/41246-sine-function-fit](https://www.mathworks.com/matlabcentral/fileexchange/41246-sine-function-fit), MATLAB Central File Exchange. Abgerufen 17. April 2024.
