#!/bin/env python
import json
import sys
import mat4py

def channel2array(channel: dict[str, float]) -> list[list[float]]:
    return [[float(k), v] for k, v in channel.items()]

def usage():
    print("""
USAGE: json2mat.py <file1.json> [file2.json ...]
    converts data outputed from the DAQ from json to mat
    and outputs the mat-file with the same name.
          """)

def rename2mat(name):
    return f"{name.split('.')[0]}.mat"

def main():
    if (nargs := len(sys.argv)) < 2:
        print("Not enough arguments")
        usage()
    nargs = nargs - 1
    for i in range(nargs):
        filename = sys.argv[i+1]
        new_filename = rename2mat(filename)
        data = []
        with open(filename, "r") as f:
            data = json.load(f)
        out = {f"channel_{k}": channel2array(v)  for k, v in data["channels"].items()}
        mat4py.savemat(new_filename, out)
    return 0

if __name__ == '__main__':
    main()
