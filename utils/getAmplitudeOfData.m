function amplitude = getAmplitudeOfData(data)
amplitude = 0.5*peak2peak(data);
end