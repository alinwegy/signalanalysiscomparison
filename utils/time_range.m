function range = time_range(time, tstart, tend)
    range = time > tstart & time <= tend;
end

