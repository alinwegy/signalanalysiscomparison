function [freq,Pxx] = getFFTofSignal(sampleTime,data)
% Ts = sampleTime;
% fs = 1/Ts;
% % Choose FFT size and calculate spectrum
% Nfft = 2048;
% [Pxx,freq] = pwelch(data,[],[],Nfft,fs);
% %[Pxx,freq] = pwelch(data,gausswin(Nfft),Nfft/2,Nfft,fs);

L = size(data,1); % Length of signal (values)
T = sampleTime;
Fs = 1/T; % Sampling frequency (Hz)
%data = lowpass(data,20000,Fs); % Filter the signal to reduce noise
X = data;
Y = fft(X'); % Compute the Fourier transform of the signal
P2 = abs(Y/L); % Compute the two-sided spectrum P2
Pxx = P2(1:L/2+1); % Compute the single-sided spectrum P1
Pxx(2:end-1) = 2*Pxx(2:end-1);
freq = Fs*(0:(L/2))/L; % Frequency domain
end