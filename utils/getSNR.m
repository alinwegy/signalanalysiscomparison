function snr = getSNR(fftPXX)
% Finding the peak in the freq spectrum
fftPXX = fftPXX(2:end); %starting at 5 to ignore 0Hz peak
[~,p] = max(fftPXX);
if p == 1
    leakDown = 0;
    leakUp = 1;
elseif p == length(fftPXX)
    leakDown = 1;
    leakUp = 0;
else
    leakDown = 1;
    leakUp = 1;
end
sigPos = p-leakDown:p+leakUp;
% finding the bins corresponding to the signal around the major peak
% including leakage
sigPow = sum(fftPXX(sigPos)); % signal power = sum of magnitudes of bins conrresponding to signal
fftPXX(sigPos) = 0; % making all bins corresponding to signal zero:==> what ever that remains is noise 
noisePow = sum(fftPXX); % sum of rest of componenents == noise power

snr = 10*log10(sigPow/noisePow);
end