function dt = t2dt(t)
  dt = t - t(end);
end
