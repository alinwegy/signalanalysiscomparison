function [freqDom,x1Dom,x2Dom,x3Dom,x4Dom,x5Dom] = getDomOfFFT(frequency,data)
startFreq = 5; %ignoring 0Hz u to 5Hz
data = data(frequency>=startFreq);
frequency = frequency(frequency>= startFreq);

[ampl,loc] = max(data);
freqDom = frequency(loc);
x1Dom = ampl;

multiplesOfFreq = [2*freqDom,3*freqDom,4*freqDom,5*freqDom];
ampDomMultiples = interp1(frequency,data,multiplesOfFreq);
x2Dom = ampDomMultiples(1);
x3Dom = ampDomMultiples(2);
x4Dom = ampDomMultiples(3);
x5Dom = ampDomMultiples(4);
end