function phaseLag = getPhaseAngleInDegree(voltage,signal,Ts,fSig)
% lowpass filter. sampling is set to 10*f_signal -> filter down
fs= 1/Ts;
signal = lowpass(signal,fs/9,fs);
voltage = lowpass(voltage,fs/9,fs);

% shift signal to 0
signal = signal - mean(signal);
voltage = voltage - mean(voltage);

% scale amplitude to 1
peaksSignal = mean(abs(findpeaks(signal)));
peaksVoltage = mean(abs(findpeaks(voltage)));
signal= signal/peaksSignal;
voltage = voltage/peaksVoltage;

% maxSig = max(abs(max(signal)),abs(min(signal)));
% maxV = max(abs(max(voltage)),abs(min(voltage))); 
% signal= signal/maxSig;
% voltage = voltage/maxV;

%% calculate lag
% Attempt 1 -> not good with noisy signal

% [c,lag]=xcorr(voltage,signal); %correlation between signals
% [~,idx] = max(c); %find average lag in samples
% sampleLag = lag(idx); %get lag in samples
% tLag =  sampleLag*Ts; %lag in seconds
% phaseLag = 180/pi*2*pi*fSig*tLag; %lag in s -> lag in rad -> lag in degree
% 
% % this algorithm reacts badly to noise.. 
% if abs(phaseLag) > 360
%     phaseLag = NaN;
% end

%Attempt 2 -> using https://dsp.stackexchange.com/questions/41291/calculating-the-phase-shift-between-two-signals-based-on-samples
% basically: dot-product of two vectors results in a new vector with different angle
phaseDiff = acos(dot(voltage,signal)) / (norm(signal)*norm(voltage));
phaseAngle = angle(phaseDiff);
phaseLag = -rad2deg(phaseAngle); %we relate all phase angles to voltage and acceleration can never be faster than voltage in these systems -> "-"


end