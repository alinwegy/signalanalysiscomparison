function freq_dom = DominantFrequency(data)
  fft_data = abs(fft(data));
  pow      = fft_data(5:end-5);
  freq_dom = max(pow);
end

