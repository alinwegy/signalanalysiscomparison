clear all; close all;
addpath(genpath('.'))

%% ================== LOAD DATA ================== %%
load('alina_v2_f600.mat')
volt  = [t2dt(channel_5(:, 1)), channel_5(:,2)];
curr  = [t2dt(channel_6(:, 1)), channel_6(:,2)];
accel = [t2dt(channel_7(:, 1)), channel_7(:,2)];

Fs        = 6000;
ts        = 2;
te        = 3;
ts_sample = ts;
te_sample = ts + 500/Fs;
volt      = volt(time_range(volt(:,1),ts, te), :);
curr      = curr(time_range(curr(:,1),ts, te), :);
accel     = accel(time_range(accel(:,1), ts, te), :);
X         = accel(:,1);
Y         = accel(:,2);
Y_sample  = accel(time_range(accel(:,1), ts_sample, te_sample), 2);
X_sample  = accel(time_range(accel(:,1), ts_sample, te_sample), 1);

%% ================== GENERATE FIT ================== %%
% amp      = (max(Y_sample) - min(Y_sample))/2;
% off      = mean(Y_sample);
% freq     = DominantFrequency(Y_sample);
% phas     = 0;

%%%%% USING THE BUILTIN FIT FUNCTION (DOES NOT WORK) %%%%%
% mdl = fittype('a*sin(b*x+c)+d','indep','x');
% fittedmdl = fit(X,Y,mdl,'start',[amp,freq,phas,off])

%%%%% USING A CUSTOM SINE_FIT FUNCTION %%%%%
params   = SineFit(X_sample,Y_sample,[],[],0);
offset = params(1)
amplitude = abs(params(2))
frequency = abs(params(4))
phase = params(3)
Y_pred   = params(2)*sin((2*pi*params(4)*X) + params(3)) + params(1);
noise    = abs(Y - Y_pred);

%%%%% USING THE BUILTIN NONLINEARMODEL FIT FUNCTION %%%%%
% sine_fiyxct = NonLinearModel.fit(X, Y, ...
%     'y ~ b0 + b1*sin(2*pi*b2*x1 + b3)', ...
%     [off, amp, freq, phas]);
% Y_pred   = sine_fit.predict(X);

%% ================== PLOT FIT ================== %%
figure
plot(X,Y)
hold on
plot(X,Y_pred)
hold off
xlabel("dt (us)")
ylabel("Acceleration (m/s^2)")
 
% %% ================== PLOT NOISE ================== %%
% figure
% plot(X, noise);
% xlabel("dt (us)");
% ylabel("noice (m/s^2)")
